dnf-server
==========

A tool for creating, updating and cleaning RPM repositories hosted on
the current system.


Usage
-----

### Create RPM repositories ###

You can create all the RPM repositories specified in the
`/etc/dnf-server.repos.d` directory with:

```bash
dnf-server create
```

### Update RPM repositories ###

You can update all the RPM repositories specified in the
`/etc/dnf-server.repos.d` directory with:

```bash
dnf-server update
```

### Clean RPM repositories ###

You can clean all the RPM repositories specified in the
`/etc/dnf-server.repos.d` directory with:

```bash
dnf-server clean
```

