dnf-server TODO
===============

- [x] Implement repolist command
- [x] Implement --repo=REPO_ID command line option
- [x] Implement add command
- [x] Covert log messages to debug (not adding debug switch as it is deprectated in dnf)
- [x] Implement list command e.g. "dnf-server --repo=REPO_ID list"
  lists all RPMs in the specified repository list all RPMs in a
  repository
- [x] Implement push and pull commands
  Add the following to repo configuration files:
  - upstream_repos: https://fqdn.net/path/to/repo
  - downstream_repos: https://fqdn.net/path/to/repo
  Add the following commands:
  - "dnf-server --repo=REPO_ID pull": pull from upstream.
  - "dnf-server --repo=REPO_ID push": push to downstream.
- [x] Implement remove command e.g. "dnf-server --repo=REPO_ID remove
  RPM_FILE1 RPM_FILE2" removes RPMs from specified repository.
- [x] Implement --skip-update option.
- [x] Modify spec file so it works for CentOS7.
- [x] Create man page.
- [ ] Implement tests.
- [ ] Fix installation directory, the current installation scripts
  seem to put the python files in a non-Fedora-standard location. It
  works, but it is not what we want in the long term.
- [ ] Improve the man page.
- [ ] Update README.md documentation.
- [ ] Create requirements section in INSTALL.md documentation.
- [ ] Refactor config.py into separate focused modules.
- [ ] Improve error handling by returning status up the call stack.
- [ ] Add example dnf-server.repos.d/REPO_ID.repo file.
- [ ] Remove RPMs which have already been specified from completions when typing remove command.
