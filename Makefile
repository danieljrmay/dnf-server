#!/usr/bin/make -f
# 
# Makefile - This is the Makefile for the dnf-server
# project.
#
# Copyright 2018 Daniel J. R. May.
#
# This file is part of dnf-server.
#
# dnf-server is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# dnf-server is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dnf-server. If not, see
# <http://www.gnu.org/licenses/>.
#

#################################
### Project related variables ###
#################################

name:=dnf-server
python_name:=dnfserver

version:=$(shell awk '/Version:/ { print $$2 }' $(name).spec)
dist_name:=$(name)-$(version)
rpm_name:=$(name)

###############################
# Standard makefile variables #
###############################

# Define the shell
SHELL:=/usr/bin/bash

# RPM commands
RPMLINT:=rpmlint

# Python commands
PYLINT:=pylint
PYTHON_SETUP:=python3 setup.py

# Install commands
INSTALL=/usr/bin/install
INSTALL_DIR=$(INSTALL) -d
INSTALL_DATA=$(INSTALL) -D -m 644
INSTALL_PROGRAM=$(INSTALL) -D -m 755

# Mock variables
DISTRO:=fedora
RELEASEVER:=$(shell rpm --eval '%{fedora}')
ARCH:=$(shell rpm --eval '%_arch')
MOCKROOT=$(DISTRO)-$(RELEASEVER)-$(ARCH)
MOCKRESULTDIR=.

# Standard makefile installation directories.
#
# The following are the standard GNU/RPM values which are defined in
# /usr/lib/rpm/macros. See
# http://www.gnu.org/software/make/manual/html_node/Directory-Variables.html
# for more information.
prefix=/usr
exec_prefix=$(prefix)
bindir=$(exec_prefix)/bin
datadir=$(prefix)/share
includedir=$(prefix)/include
infodir=$(prefix)/info
libdir=$(exec_prefix)/lib
libexecdir=$(exec_prefix)/libexec
localstatedir=$(prefix)/var
mandir=$(prefix)/man
rpmconfigdir=$(libdir)/rpm
sbindir=$(exec_prefix)/sbin
sharedstatedir=$(prefix)/com
sysconfdir=$(prefix)/etc

# If the install_dirs variable equals "rhel" (this is set as default)
# then we override some of the previous GNU standards with the Red Hat
# style values as defined in /usr/lib/rpm/redhat/macros.
install_dirs=rhel
ifeq ($(install_dirs), rhel)
sysconfdir=/etc
defaultdocdir=/usr/share/doc
infodir=/usr/share/info
initrddir=$(sysconfdir)/rc.d/init.d
localstatedir=/var
mandir=/usr/share/man
sharedstatedir=$(localstatedir)/lib
webassetdir=$(datadir)/web-assets
jsdir=$(datadir)/javascript
endif


####################
# Makefile targets #
####################

# Build all components. This is the default target. This redirectes to
# the 'build' target of the project-specific makefile.
.PHONY: all
all: $(name).1.gz
	$(info all:)
	$(info To build the $(python_name) module use the pybuild target, e.g. make pybuild)

$(name).1.gz: man/$(name).1
	gzip --best --stdout man/$(name).1 > $(name).1.gz

# Pre-build check. This checks source files before they are used to
# compile or otherwise create build files.
.PHONY: precheck
precheck:
	$(info precheck:)	
	$(RPMLINT) $(name).spec
	$(PYLINT) $(python_name)
	$(SHELL) -n $(name).completion.bash

.PHONY: pybuild
pybuild:
	$(info pybuild:)
	$(PYTHON_SETUP) build

# Post-build, pre-installation check. This checks build files.
.PHONY: check
check:
	$(info check:)
	$(info To check the $(python_name) module use the pycheck target, e.g. make pycheck)

# Post-build, pre-installation check of the python module.
.PHONY: pycheck
pycheck:
	$(info pycheck:)
	$(PYTHON_SETUP) check

# Create a test installation directory and set DESTDIR equal to it.
testdestdir:
	mkdir testdestdir
	$(eval DESTDIR:=testdestdir)

# Perform a test installation
.PHONY: testinstall
testinstall: | testdestdir install installcheck

# Install the built program.
.PHONY: install
install: all
	$(info install:)
	$(INSTALL_DATA) $(name).completion.bash $(DESTDIR)$(datadir)/bash-completion/completions/$(name)
	$(INSTALL_DATA) $(name).1.gz $(DESTDIR)$(mandir)/man1/$(name).1.gz

# Install the python module.
.PHONY: pyinstall
pyinstall: 
	$(info pyinstall:)
	$(PYTHON_SETUP) install

# Check the installation.
.PHONY: installcheck
installcheck:
	$(info installcheck:)
	test -r $(DESTDIR)$(datadir)/bash-completion/completions/$(name)
	test -r $(DESTDIR)$(mandir)/man1/$(name).1.gz

# Uninstall the program.
.PHONY: uninstall
uninstall:
	$(info uninstall:)
	rm -f $(DESTDIR)$(datadir)/bash-completion/completions/$(name)
	rm -f $(DESTDIR)$(mandir)/man1/$(name).1.gz

#Uninstall the python module

# Generate a distribution tarball
.PHONY: dist
dist:
	$(info dist:)
	mkdir -p $(dist_name)
	cp -r --target-directory $(dist_name) AUTHORS dnfserver Makefile man *.bash *.md *.py *.spec *.txt
	tar --create --gzip --exclude='$(python_name)/__pycache__' --file $(dist_name).tar.gz $(dist_name)
	rm -rf $(dist_name)

# Generate a SRPM
.PHONY: srpm
srpm: dist
	$(info srpm:)
	mock --root=$(MOCKROOT) --resultdir=$(MOCKRESULTDIR) --buildsrpm \
		--spec $(rpm_name).spec --sources $(dist_name).tar.gz

# Generate a RPM
.PHONY: rpm
rpm: srpm
	$(info rpm:)
	mock --root=$(MOCKROOT) --resultdir=$(MOCKRESULTDIR) --rebuild $(rpm_name)*.src.rpm

# Clean all generated binary files
.PHONY: clean
clean:
	$(info clean:)
	rm -f *.rpm *.tar.gz *.log *.1.gz
	rm -rf build dist $(python_name).egg-info
	rm -rf $(python_name)/__pycache__

# Clean up to restore sources to their pristine state as if you had just unpacked the sources
.PHONY: distclean
distclean: clean
	$(info distclean:)
	find . -name '*~' -delete
	rm -rf testdestdir 

# Help for common tasks
.PHONY: help
help:
	$(info help:)
	$(info Usage: make TARGET [VAR1=VALUE VAR2=VALUE])
	$(info )
	$(info Targets:)
	$(info   all             The default target, redirects to build.)
	$(info   precheck        Pre-build check of source files.)
	$(info   check           Post-build check of all built files.)
	$(info   testinstall     Perform a test installation.)
	$(info   install         Install.)
	$(info   installcheck    Post-installation check of all installed files.)
	$(info   uninstall       Uninstall.)
	$(info   dist		 Create a distribution tarball.)
	$(info   srpm		 Create a SRPM.)
	$(info   rpm		 Create a RPM.)
	$(info   clean           Clean up all generated binary files.)
	$(info   distclean       Clean up all generated files.)
	$(info   help            Display this help message.)
	$(info   printvars       Print variable values (useful for debugging).)
	$(info   printmakevars   Print the Make variable values (useful for debugging).)
	$(info )
	$(info Variables:)
	$(info   DESTDIR          Used with the install target to check the installation root.=)
	$(info   install_dirs     One of {gnu, rhel}, sets the installation directory variables. Defaults to 'rhel'.)
	$(info )
	$(info For more information read the Makefile and see http://www.gnu.org/software/make/manual/html_node/index.html)

# Print user defined makefile variables
.PHONY: printvars
printvars:
	$(info printvars:)
	$(info name=$(name))
	$(info version=$(version))
	$(info INSTALL=$(INSTALL))
	$(info INSTALL_DATA=$(INSTALL_DATA))
	$(info INSTALL_DIR=$(INSTALL_DIR))
	$(info INSTALL_PROGRAM=$(INSTALL_PROGRAM))
	$(info MOCKROOT=$(MOCKROOT))
	$(info MOCKRESULTDIR=$(MOCKRESULTDIR))
	$(info prefix=$(prefix))
	$(info exec_prefix=$(exec_prefix))
	$(info bindir=$(bindir))
	$(info datadir=$(datadir))
	$(info includedir=$(includedir))
	$(info infodir=$(infodir))
	$(info libdir=$(libdir))
	$(info libexecdir=$(libexecdir))
	$(info localstatedir=$(localstatedir))
	$(info mandir=$(mandir))
	$(info rpmconfigdir=$(rpmconfigdir))
	$(info sbindir=$(sbindir))
	$(info sharedstatedir=$(sharedstatedir))
	$(info sysconfdir=$(sysconfdir))

# Print all makefile variables
.PHONY: printmakevars
printmakevars:
	$(info printmakevars:)
	$(info $(.VARIABLES))
