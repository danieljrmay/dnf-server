#!/usr/bin/env python3

"""Tests the config.py module."""

import logging
import unittest

from dnfserver.config import get_config_file_paths
#from dnfserver.config import replace_variables

class TestConfigModule(unittest.TestCase):
    def setUp(self):
        self.logger = logging.getLogger('dnf-server-testing')
        self.logger.addHandler(logging.StreamHandler())
        self.logger.setLevel(level=logging.DEBUG)

    def test_get_config_file_paths(self):
        repos_config_dir = 'non-existant-directory'
        try:
            get_config_file_paths(repos_config_dir, self.logger)
        except Exception as e:
            self.assertEqual(
                'The repository configuration directory "' + repos_config_dir \
                + '" is not a readable directory.', str(e))

        repos_config_dir = 'existant-directory'
        try:
            get_config_file_paths(repos_config_dir, self.logger)
        except Exception as e:
            self.assertEqual(
                'The repository configuration directory "' + repos_config_dir \
                + '" is not a readable directory.', str(e))



    #def test_replace_variables(self):
    #    mystring = 'no variables to replace'
    #    replacements = {
    #        'arch': 'x86_64',
    #        'basearch': 'x86_64',
    #        'releasever': '28'
    #    }        
    #    self.assertEqual(replace_variables(mystring, replacements), mystring)


if __name__ == '__main__':
    unittest.main()



# rpmrepoctl

