#!/usr/bin/env python3

"""A tool for managing RPM repositories hosted on the local system."""

import logging

from dnfserver.cli import get_command_line_arguments, process_command_line_arguments
from dnfserver.config import process_repo_config_files

__author__ = "Daniel J. R. May"
__copyright__ = "Copyright 2018, Daniel J. R. May"
__credits__ = ["Daniel J. R. May"]
__license__ = "GPLv3"
__version__ = "0.2.3"
__maintainer__ = "Daniel J. R. May"
__email__ = "daniel.may@danieljrmay.com"
__status__ = "Prototype"

# Debugging mode: assign debug to True to force output of all
# debugging messages regardless of any verbosity command line options.
DEBUGGING_MODE = False


# Set up logging
LOGGER = logging.getLogger('dnf-server')
LOGGER.addHandler(logging.StreamHandler())

if DEBUGGING_MODE:
    LOGGER.setLevel(level=logging.DEBUG)
else:
    LOGGER.setLevel(level=logging.INFO)

LOGGER.debug('dnf-server started')


# Get and process the command line arguments
ARGS = get_command_line_arguments(LOGGER)
process_command_line_arguments(ARGS, DEBUGGING_MODE, LOGGER)


# Process the repository configuration files
process_repo_config_files(ARGS, LOGGER)


# Exit
LOGGER.debug('dnf-server finished')
exit(0)
