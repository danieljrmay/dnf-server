#!/usr/bin/env python3

"""Createrepo related code."""

import os
import subprocess


def create(name, distro, content, tags, path, logger):
    """Create a repository."""

    # Create directory if it does not exist
    if not os.path.exists(path):
        logger.debug('Creating directory ' + path)
        os.makedirs(path)

    # Create the repository
    logger.info('Creating "' + name + '" at ' + path)
    subprocess.run(['createrepo_c', '--quiet', '--distro', distro, '--content', content, '--repo', tags, path], check=True)


def update(name, distro, content, tags, path, logger):
    """Update a repository."""

    # Check repository directory already exists
    if not os.path.isdir(path + '/repodata'):
        logger.error('No repository exists at ' + path)
        logger.error('You must create a repository before you can update it')
        exit(2)

    # Update the repository
    logger.info('Updating "' + name + '" at ' + path)
    subprocess.run(['createrepo_c', '--update', '--quiet', '--distro', distro, '--content', content, '--repo', tags, path], check=True)


def clean(name, path, skip_update, logger):
    """Clean a repository."""

    # Check repository directory already exists
    if not os.path.isdir(path + '/repodata'):
        logger.error('No repository exists at ' + path)
        logger.error('You must create a repository before you can clean it')
        exit(3)

    logger.info('Cleaning "' + name + '" at ' + path)
    result = subprocess.run(
        ['repomanage', '--keep=2', '--old', path],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )

    # Check if there were old RPM files which need to be removed
    if result.stderr.decode('utf-8') == 'Error: No files to process\n':
        logger.debug('No RPM files in repository at ' + path)
    else:
        old_rpm_paths = result.stdout.decode('utf-8').splitlines()
        update_required = False

        for rpm_path in old_rpm_paths:
            logger.info('Removing old RPM ' + rpm_path)
            os.remove(rpm_path)
            update_required = True

        if update_required and not skip_update:
            update(name, path, logger)


def remote_update(name, distro, content, tags, downstream_url, logger):
    """Run update on remote repository."""

    user_at_host, remote_path = downstream_url.split(':', 1)
    logger.debug('user_at_host=' + user_at_host)
    logger.debug('remote_path=' + remote_path)

    # Update the remote repository
    logger.info('Updating remote repository ' + downstream_url)
    cmd = 'ssh ' + user_at_host + " 'createrepo_c --update --quiet --distro " + distro + " --content " + content + " --repo " + tags + " " + remote_path + "'"
    logger.debug('Executing cmd=' + cmd)
    status = subprocess.run(cmd, shell=True)
    if status.returncode == 0:
        logger.debug('Successfully updated remote repository ' + downstream_url)
    else:
        logger.error('Failed to update remote repository ' + downstream_url)
