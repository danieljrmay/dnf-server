#!/usr/bin/env python3

"""Repository configuration file handling code."""

import configparser
import glob
import os
from shutil import copy2

import dnf

from dnfserver.createrepo import clean, create, remote_update, update
from dnfserver.rsync import pull, push


def get_config_file_paths(repos_config_dir, logger):
    """
    Return an array of paths to the repository configuration files.
    """
    if not (os.path.isdir(repos_config_dir) and os.access(repos_config_dir, os.R_OK)):
        msg = 'The repository configuration directory "' + repos_config_dir \
              + '" is not a readable directory.'
        logger.critical(msg)
        raise Exception(msg)
        
    config_files = glob.glob(config_dir + '/*.repo')

    if not config_files:
        msg = 'No *.repo repository configuration files found in ' + config_dir
        logger.warning(msg)
        raise Exception(msg)

    logger.debug('Found the following repository configuration files '
                 + str(config_files))

    return config_files


def replace_variables(my_string, replacements):
    """
    Replace the variables which appear in the repository configuration
    files like $basearch -> x86_64.
    """

    for variable_name, replacement in replacements.items():
        my_string = my_string.replace('$' + variable_name, replacement)

    return my_string


def get_path_from_file_uri(file_uri):
    """Get an absolute path from a file uri."""

    if file_uri.startswith('file://'):
        return os.path.abspath(file_uri[7:])
    else:
        assert False, 'Supplied argument was not a file uri'


def get_list_from_csv(csv):
    """Get a list from a comma separated string of values."""

    # Split string into list on commas
    untrimmed_list = csv.split(',')

    # Trim whitespace from every element of the list
    trimmed_list = [x.strip() for x in untrimmed_list]

    # Remove empty strings (and every other element which evaluateds
    # to False) from the list
    cleaned_list = list(filter(None, trimmed_list))

    return cleaned_list


def process_repo_config_files(args, logger):
    """Process the repository configuration files."""

    # Get the repository configuration file paths
    config_files = get_config_file_paths(logger)

    # Get a dictonary of dnf subsitutions so that we can replace things
    # like "$basearch" with "x86_64", etc.
    replacements = dnf.dnf.Base().conf.substitutions
    logger.debug('dnf replacements dictonary ' + str(replacements))

    # Construct a configuration file parser
    config = configparser.ConfigParser()

    # Parse and process each repository configuration files
    for config_path in config_files:
        logger.debug('Reading ' + config_path)
        config.read(config_path)

    # Execute command
    if args.command == 'add':
        execute_add(config, replacements, args, logger)
    elif args.command == 'clean':
        execute_clean(config, replacements, args, logger)
    elif args.command == 'create':
        execute_create(config, replacements, args, logger)
    elif args.command == 'list':
        execute_list(config, replacements, args, logger)
    elif args.command == 'pull':
        execute_pull(config, replacements, args, logger)
    elif args.command == 'push':
        execute_push(config, replacements, args, logger)
    elif args.command == 'remove':
        execute_remove(config, replacements, args, logger)
    elif args.command == 'repolist':
        execute_repolist(config, replacements, args, logger)
    elif args.command == 'update':
        execute_update(config, replacements, args, logger)
    else:
        assert False, 'Illegal command'


def get_repo_id_list_to_process(config, args, logger):
    """Get a list of repo_ids to process."""

    if args.repo is None:
        repo_ids = config.sections()
    else:
        repo_ids = args.repo

    logger.debug('Repo IDs list to process = ' + str(repo_ids))
    return repo_ids


def check_rpm_files_permissions(rpm_paths_list, permission, logger):
    """Check that a list RPM files have the specified permission."""

    for path in rpm_paths_list:
        if os.path.splitext(path)[1] != '.rpm':
            logger.error('The specified path does not have a .rpm extension: ' + path)
            exit(1)

        if not os.path.isfile(path):
            logger.error('The specified RPM path does not exist: ' + path)
            exit(1)

        if not os.access(path, permission):
            logger.error('The specified RPM path does not have the required permission: ' + path)
            exit(1)


def get_user_confirmation(question, possible_answers, default_answer):
    """Get confirmation of an action from the user."""

    answer = input(question)

    if answer in possible_answers:
        return answer

    return default_answer


def execute_add(config, replacements, args, logger):
    """Execute the add command."""

    # Check specified RPMs exist and are readable
    check_rpm_files_permissions(args.rpms, os.R_OK, logger)

    # Get confirmation if adding to all
    if args.repo is None:
        possible_confirmations = ['yes', 'y']
        confirmation = get_user_confirmation(
            'Are you sure that you want to add the specified RPMs to EVERY repository? (yes/no): ',
            possible_confirmations,
            'no'
        )

        if confirmation in possible_confirmations:
            logger.info('User confirmed addition of specified RPMs to EVERY repository.')
        else:
            logger.info('Exiting. No action has been taken.')
            exit(0)

    repo_id_list = get_repo_id_list_to_process(config, args, logger)

    for repo_id in repo_id_list:
        logger.debug('Excuting add on ' + repo_id)
        repo_name = replace_variables(config[repo_id]['name'], replacements)
        repo_baseurl = replace_variables(config[repo_id]['baseurl'], replacements)
        repo_path = get_path_from_file_uri(repo_baseurl)

        for rpm_path in args.rpms:
            logger.info('Copying ' + rpm_path + ' into ' + repo_path)
            copy2(rpm_path, repo_path)

        if not args.skip_update:
            update(repo_name, repo_path, logger)


def execute_clean(config, replacements, args, logger):
    """Execute the clean command."""

    repo_id_list = get_repo_id_list_to_process(config, args, logger)

    for repo_id in repo_id_list:
        logger.debug('Excuting clean on ' + repo_id)
        repo_name = replace_variables(config[repo_id]['name'], replacements)
        repo_baseurl = replace_variables(config[repo_id]['baseurl'], replacements)
        repo_path = get_path_from_file_uri(repo_baseurl)
        clean(repo_name, repo_path, args.skip_update, logger)


def execute_create(config, replacements, args, logger):
    """Execute the create command."""

    repo_id_list = get_repo_id_list_to_process(config, args, logger)

    for repo_id in repo_id_list:
        logger.debug('Excuting create on ' + repo_id)
        repo_name = replace_variables(config[repo_id]['name'], replacements)
        repo_baseurl = replace_variables(config[repo_id]['baseurl'], replacements)
        repo_path = get_path_from_file_uri(repo_baseurl)
        create(repo_name, repo_path, logger)


def execute_list(config, replacements, args, logger):
    """Execute the list command."""

    repo_id_list = get_repo_id_list_to_process(config, args, logger)

    for repo_id in repo_id_list:
        logger.debug('Excuting list on ' + repo_id)
        repo_baseurl = replace_variables(config[repo_id]['baseurl'], replacements)
        repo_path = get_path_from_file_uri(repo_baseurl)
        rpm_paths_list = glob.glob(repo_path + '/*.rpm')

        for rpm_path in rpm_paths_list:
            print('%-60s %20s' % (os.path.basename(rpm_path), repo_id))


def execute_pull(config, replacements, args, logger):
    """Execute the pull command."""

    repo_id_list = get_repo_id_list_to_process(config, args, logger)

    for repo_id in repo_id_list:
        logger.debug('Excuting pull on ' + repo_id)
        repo_name = replace_variables(config[repo_id]['name'], replacements)
        repo_baseurl = replace_variables(config[repo_id]['baseurl'], replacements)
        repo_path = get_path_from_file_uri(repo_baseurl)
        upstream_repos_csv = replace_variables(
            config[repo_id].get('upstream_repos', ''),
            replacements
        )
        upstream_repos_list = get_list_from_csv(upstream_repos_csv)

        logger.debug('Upstream repos ' + str(upstream_repos_list))

        for upstream_url in upstream_repos_list:
            pull(repo_name, repo_path, upstream_url, logger)

            if not args.skip_update:
                update(repo_name, repo_path, logger)


def execute_push(config, replacements, args, logger):
    """Execute the push command."""

    repo_id_list = get_repo_id_list_to_process(config, args, logger)

    for repo_id in repo_id_list:
        logger.debug('Excuting push on ' + repo_id)
        repo_name = replace_variables(config[repo_id]['name'], replacements)
        repo_baseurl = replace_variables(config[repo_id]['baseurl'], replacements)
        repo_path = get_path_from_file_uri(repo_baseurl)
        downstream_repos_csv = replace_variables(
            config[repo_id].get('downstream_repos', ''),
            replacements
        )
        downstream_repos_list = get_list_from_csv(downstream_repos_csv)

        logger.debug('Downstream repos ' + str(downstream_repos_list))

        for downstream_url in downstream_repos_list:
            push(repo_name, repo_path, downstream_url, logger)

            if not args.skip_update:
                remote_update(downstream_url, logger)


def execute_remove(config, replacements, args, logger):
    """Execute the remove command."""

    # Get confirmation if removing from all
    if args.repo is None:
        possible_confirmations = ['yes', 'y']
        confirmation = get_user_confirmation(
            'Are you sure that you want to remove the specified RPMs ' +
            'from EVERY repository? (yes/no): ',
            possible_confirmations,
            'no'
        )

        if confirmation in possible_confirmations:
            logger.info('User confirmed removal of specified RPMs from EVERY repository.')
        else:
            logger.info('Exiting. No action has been taken.')
            exit(0)

    repo_id_list = get_repo_id_list_to_process(config, args, logger)

    for repo_id in repo_id_list:
        logger.debug('Excuting remove on ' + repo_id)
        repo_name = replace_variables(config[repo_id]['name'], replacements)
        repo_baseurl = replace_variables(config[repo_id]['baseurl'], replacements)
        repo_path = get_path_from_file_uri(repo_baseurl)

        for rpm in args.rpms:
            path = repo_path + '/' + rpm

            if os.path.isfile(path):
                logger.info('Removing ' + rpm + ' from ' + repo_id)
                os.remove(path)
            else:
                logger.info(rpm + ' does not exist in ' + repo_id)

        if not args.skip_update:
            update(repo_name, repo_path, logger)


def execute_repolist(config, replacements, args, logger):
    """Execute the repolist command."""

    # We execute repolist on every repo irrespective of the value of ars.repos
    repo_id_list = config.sections()

    for repo_id in repo_id_list:
        logger.debug('Excuting repolist on ' + repo_id)

        if args.verbose:
            print('Repo-id: ' + repo_id)
            repo_name = replace_variables(config[repo_id]['name'], replacements)
            print('Repo-name: ' + repo_name)
            repo_baseurl = replace_variables(config[repo_id]['baseurl'], replacements)
            print('Repo-baseurl: ' + repo_baseurl)
            repo_path = get_path_from_file_uri(repo_baseurl)
            print('Repo-path: ' + repo_path)
            print('')
        else:
            print(repo_id)


def execute_update(config, replacements, args, logger):
    """Execute the repolist command."""

    repo_id_list = get_repo_id_list_to_process(config, args, logger)

    for repo_id in repo_id_list:
        logger.debug('Excuting update on ' + repo_id)
        repo_name = replace_variables(config[repo_id]['name'], replacements)
        repo_baseurl = replace_variables(config[repo_id]['baseurl'], replacements)
        repo_path = get_path_from_file_uri(repo_baseurl)
        update(repo_name, repo_path, logger)
