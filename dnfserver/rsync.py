#!/usr/bin/env python3

"""Rsync related code."""

import os
import subprocess


def pull(name, path, upstream_url, logger):
    """Pull from upstream repositories."""

    # Create directory if it does not exist
    if not os.path.exists(path):
        logger.debug('Creating directory ' + path)
        os.makedirs(path)

    # Rsync from the upstream repository
    logger.info('Pulling from upstream ' + upstream_url + ' into "' + name + '" at ' + path)
    cmd = 'rsync -vh ' + upstream_url + '/*.rpm ' + path
    status = subprocess.run(cmd, shell=True)
    if status.returncode == 0:
        logger.debug('Successfully pulled from upstream' + upstream_url)
    else:
        logger.error('Failed to pull from upstream ' + upstream_url)


def push(name, path, downstream_url, logger):
    """Push to downstream repositories."""

    # Create directory if it does not exist
    if not os.path.exists(path):
        logger.debug('Creating directory ' + path)
        os.makedirs(path)

    # Rsync to downstream repository
    logger.info('Pushing to downstream ' + downstream_url + ' from "' + name + '" at ' + path)
    cmd = 'rsync -vh ' + path + '/*.rpm ' + downstream_url
    status = subprocess.run(cmd, shell=True)
    if status.returncode == 0:
        logger.debug('Successfully pushed to downstream' + downstream_url)
    else:
        logger.error('Failed to push to downstream ' + downstream_url)
