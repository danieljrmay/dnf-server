#!/usr/bin/env python3

"""Command line interface related code."""

import argparse
import logging

def get_command_line_arguments(logger):
    """
    Parse the command line arguments and return them as Namespace
    object from the argparse module.
    """

    logger.debug('Parsing command line arguments')
    parser = argparse.ArgumentParser(
        prog='dnf-server',
        description='A tool for managing RPM repositories hosted on the local system.',
        epilog='For more information see https://github.com/danieljrmay/dnf-server'
    )

    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        default=False,
        help='Verbose operation, show lots of messages.'
    )

    parser.add_argument(
        '-y', '--assumeyes',
        action='store_true',
        default=False,
        help='Automatically answer yes for all questions.'
    )

    parser.add_argument(
        '--skip-update',
        action='store_true',
        default=False,
        help='Skip repository update after add, remove and pull commands.'
    )

    parser.add_argument(
        '--repo',
        action='append',
        default=None,
        help='Operate on the specified repository.'
    )

    parser.add_argument(
        '--releasever',
        action='append',
        default=None,
        help='Specify the value of the $releasever variable.'
    )

    parser.add_argument(
        '--basearch',
        action='append',
        default=None,
        help='Specify the value of the $basearch variable.'
    )

    parser.add_argument(
        '--repos-config-dir',
        action='append',
        default='/etc/dnf-server.repos.d',
        help='Specify the value of the $basearch variable.'
    )  

    parser.add_argument(
        'command',
        action='store',
        choices=['add', 'clean', 'create', 'list', 'pull', 'push', 'remove', 'repolist', 'update'],
        help='The command to execute.'
    )

    parser.add_argument(
        'rpms',
        action='store',
        nargs=argparse.REMAINDER,
        default=None,
        help='The RPM files to operate on.'
    )

    return parser.parse_args()


def process_command_line_arguments(args, debugging_mode, logger):
    """
    Process the the command line arguments.
    """

    logger.debug('Setting logging level')
    if not debugging_mode and args.verbose:
        logger.setLevel(logging.DEBUG)

    logger.debug(
        'Running dnf-server with the following arguments:\n\t' +
        'verbose = ' + str(args.verbose) + '\n\t' +
        'assumeyes = ' + str(args.assumeyes) + '\n\t' +
        'skip_update = ' + str(args.skip_update) + '\n\t' +
        'repos = ' + str(args.repo) + '\n\t' +
        'releasever = ' + str(args.releasever) + '\n\t' +
        'basearch = ' + str(args.basearch) + '\n\t' +
        'command = ' + args.command + '\n\t' +
        'rpms = ' + str(args.rpms)
    )
