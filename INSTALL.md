dnf-server Installation Guide
=============================


Python setuptools based installation
------------------------------------

You can use Python's setuptools to install from source code with:

```bash
python setup.py install
```


RPM based installation using dnf or yum
---------------------------------------

You will be able to install dnf-server as package as an RPM package
once it is released.


### dnf ###

If you have dnf on your system then you can install with:

```bash
dnf copr enable danieljrmay/dnf-server
dnf install dnf-server
```

### yum ###

If you have yum on your system then you can install with:

```bash
yum copr enable danieljrmay/dnf-server
yum install dnf-server
```
