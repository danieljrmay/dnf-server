# Bash Completion scriptlet for dnf-server
#
# Author: Daniel J. R. May <daniel.may@danieljrmay.com>
# Version: 0.2.3

function _dnf-server()
{
    local cur prev words cword
    _init_completion || return

    local long_options short_options commands

    long_options='--help --verbose --repo --skip-update'
    short_options='-h -v'
    commands='add clean create list pull push remove repolist update'

    case $prev in
	dnf-server|-v|--verbose)
	    COMPREPLY=( $(compgen -W "${long_options} ${short_options} ${commands}" -- ${cur}) )
	    return
	    ;;
	-h|--help|clean|create|list|pull|push|repolist|update)
	    return
	    ;;
	'--repo')
	    local repoids=$(dnf-server repolist)
	    COMPREPLY=( $(compgen -W "${repoids}" -- ${cur}) )
	    return
	    ;;
	add)
	    _filedir rpm
	    return
	    ;;
	remove)
	    local repoargs=''

	    for i in "${!words[@]}"
	    do
		if [[ ${words[$i]} == '--repo' ]]
		then
		    repoid=${words[$(($i + 1))]}
		    repoargs+="--repo $repoid "
		fi
	    done

	    local rpms=$(dnf-server ${repoargs} list | awk '{print $1}')
	    COMPREPLY=( $(compgen -W "${rpms}" -- ${cur}) )
	    return
	    ;;
    esac

    if [[ $cur = -* ]]
    then
	COMPREPLY=( $(compgen -W "${long_options} ${short_options}" -- "$cur") )
	return
    fi

    if [[ "${words[@]}" =~ "add" ]]
    then
	_filedir rpm
	return
    fi

    if [[ "${words[@]}" =~ "remove" ]]
    then
	repoargs=''

	for i in "${!words[@]}"
	do
	    if [[ ${words[$i]} == '--repo' ]]
	    then
		repoid=${words[$(($i + 1))]}
		repoargs+="--repo $repoid "
	    fi
	done

	rpms=$(dnf-server ${repoargs} list | awk '{print $1}')
	COMPREPLY=( $(compgen -W "${rpms}" -- ${cur}) )
	return
    fi


    COMPREPLY=( $(compgen -W "${long_options} ${short_options} ${commands}" -- ${cur}) )
}
complete -F _dnf-server dnf-server
