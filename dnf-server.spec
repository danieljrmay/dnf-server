Name:           dnf-server
Version:        0.2.3
Release:        1%{?dist}
Summary:        Tool for managing locally hosted RPM repositories

License:        GPLv3
URL:            https://github.com/danieljrmay/%{name}
Source0:        https://github.com/danieljrmay/%{name}/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python%{python3_pkgversion}-devel python%{python3_pkgversion}-setuptools
Requires:       createrepo_c openssh-clients python3-dnf-server rsync

%{?fedora:Recommends:     bash-completion}

%description
A tool for managing RPM repositories hosted on the local system. When
a repository family is defined in a
/etc/dnf-server.repos.d/REPO_FAMILY.repo file, the tool can currentlly
be used to easily create, update and clean those repositories. The
syntax of the repository definition files is as close as possible to
the /etc/yum.repos.d/REPO_FAMILY.repo files.

%package     -n python3-%{name}
Summary:        Python 3 components for dnf-server, a tool for managing locally hosted RPM repositories

%description -n python3-%{name}
The dnfserver Python 3 module which provides the core functionality
for the dnf-server tool.

%prep
%autosetup -n %{name}-%{version}

%build
make
%py3_build

%install
%make_install
%py3_install

#%check
#%{__python3} setup.py test

%files -n python3-%{name}
%{python3_sitelib}/*
%doc AUTHORS README.md INSTALL.md
%license LICENSE.txt

%files
%{_bindir}/%{name}
%{_datadir}/bash-completion/completions/%{name}
%{_mandir}/man1/%{name}.1.gz
%doc AUTHORS README.md INSTALL.md
%license LICENSE.txt

%changelog
* Thu Jun 14 2018 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.2.3-1
- Fix error handling in subprocess calls

* Mon Jun  4 2018 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.2.2-1
- Correct author email address 
- Add man page

* Thu May 24 2018 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.2.1-1
- Modify spec file so it works for CentOS7

* Wed May 23 2018 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.2.0-1
- Implement --repo and --skip-update options
- Implement add, list, push, pull, remove and repolist commands
- Covert log messages to debug level
- Convert TODO to markdown
- Fix bash completions when verbose switch is used

* Thu May 10 2018 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.1.4-1
- Turn off hard-coded debugging
- Update all version numbers

* Thu May 10 2018 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.1.3-1
- Add bash-completion recommendation in RPM spec

* Thu May 10 2018 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.1.2-1
- Update Bash completion script to conform to standards
- Fix requirements in RPM spec

* Tue May  8 2018 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.1.1-1
- Initial release

